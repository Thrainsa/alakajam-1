﻿using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : AbstractEnemyController {

    public int maxScale = 3;
    private int scale;

    // Use this for initialization
    void Start()
    {
        speed = UnityEngine.Random.Range(minSpeed, maxSpeed);
        rigibody = GetComponent<Rigidbody>();
        rigibody.velocity = Vector3.left * speed;
        scale = Random.Range(1, maxScale + 1);
        transform.localScale *= scale;

        GetComponent<Damageable>().hitPoints *= scale;
    }

    protected new void SpawnElements(List<SpawnElement> spawnElements)
    {
        List<SpawnElement> part = spawnElements.GetRange(0, System.Math.Min(spawnElements.Count, scale));
        base.SpawnElements(part);
    }
}
