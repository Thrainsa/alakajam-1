﻿using System;
using UnityEngine;

[Serializable]
public class SpawnElement
{
    public GameObject bonusComponentPrefab;
}