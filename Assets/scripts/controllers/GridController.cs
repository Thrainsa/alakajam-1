﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridController : MonoBehaviour {

    public Vector3 startPoint;
    public GameObject gridTilePrefab;
    public int width;
    public int height;


    private List<GameObject> tiles = new List<GameObject>();

	// Use this for initialization
	void Start () {
        Vector3 tileSize = Vector3.Scale(gridTilePrefab.GetComponent<MeshFilter>().sharedMesh.bounds.size, gridTilePrefab.transform.localScale);
        Vector3 gridSize = new Vector3(width * tileSize.x, 0, height * tileSize.z);

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                Vector3 position = new Vector3(startPoint.x + i * tileSize.x, startPoint.y, startPoint.z + j * tileSize.z);
                GameObject tileClone = (GameObject)Instantiate(gridTilePrefab, position ,Quaternion.identity);
                GridTileController tile = tileClone.GetComponent<GridTileController>();
                tile.minX = -90;
                tile.alphaBoundaries = new Boundary(-67f,14f,-20f,35f);
                tile.resetOffset = gridSize.x;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
