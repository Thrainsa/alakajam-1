﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Ennemie 2 : je fonce sur le joueur au tout début seulement.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class Enemy2Controller : AbstractEnemyController {

    private Vector3 direction;
    private float nextPossibleFire = 0f;

    // Use this for initialization
    void Start ()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");

        speed = Random.Range(minSpeed, maxSpeed);
        if (player)
        {
            direction = (player.transform.position - gameObject.transform.position).normalized;
        } else
        {
            direction = Vector3.left;
        }
        rigibody = GetComponent<Rigidbody>();
        rigibody.velocity = direction * speed;
    }

    private void FixedUpdate()
    {
        if (Time.fixedTime >= nextPossibleFire && ShouldFire())
        {
            FireLaser(regularLaser, transform.position, direction);
            nextPossibleFire = Time.fixedTime + Random.Range(fireInterval * 2/3, fireInterval * 4/3);
        }
    }

    bool ShouldFire()
    {
        var player = GameObject.FindGameObjectWithTag("Player");

        return player && (player.transform.position - transform.position).magnitude <= weaponRange;
    }
}
