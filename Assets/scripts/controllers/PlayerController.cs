﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour, CanDie {

    public float speed;
    public float tilt;
    public Boundary boundary;
    public GameObject regularLaser;

    private Rigidbody rb;
    private float nextPossibleFire = 0f;
    private Damageable damageable;
    private ShieldComponent shieldComponent;
    private float invicibilityEnd;

    // Elements
    private Dictionary<ElementType, ElementData> alchemyPoints = new Dictionary<ElementType, ElementData>();
    private int currentGold = 0;
    private float nextAirGeneration;

    // Elements bonus
    public List<FireBonus> fireBonusDataList = new List<FireBonus>();
    public List<AirBonus> airBonusDataList = new List<AirBonus>();

    // Audio
    public AudioSource shotAudioSource;
    public AudioSource coinAudioSource;
    public AudioSource bonusAudioSource;

    public int maxHitPoints;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        damageable = GetComponent<Damageable>();
        foreach(ElementType type in Enum.GetValues(typeof(ElementType)))
        {
            if (type != ElementType.GOLD)
            {
                alchemyPoints.Add(type, new ElementData());
            }
        }
        alchemyPoints[ElementType.FIRE].maxLevel = fireBonusDataList.Count;
        alchemyPoints[ElementType.AIR].maxLevel = airBonusDataList.Count;
        alchemyPoints[ElementType.EARTH].level = 2;
        alchemyPoints[ElementType.EARTH].maxLevel = 2;
        alchemyPoints[ElementType.WATER].maxLevel = 1;
        damageable.SetWaterShield(alchemyPoints[ElementType.WATER]);
        nextAirGeneration = Time.time + 1;
        invicibilityEnd = Time.time;

        shieldComponent = GetComponentInChildren<ShieldComponent>();
    }

    public void Hurt()
    {
        invicibilityEnd = Time.time + 0.5f;
    }

    void Update()
    {
        if(nextAirGeneration < Time.time)
        {
            bool levelUp = alchemyPoints[ElementType.AIR].Add(1);
            if (levelUp)
            {
                Debug.Log(ElementType.AIR.ToString() + " level up!");
            }
            nextAirGeneration += 0.15f;
        }
        /*
        if (Time.time < invicibilityEnd)
        {
            float alpha = (Mathf.Sin(Time.time * 3) + 1) / 4 + 0.25f;
            ChangeAlpha(alpha);
        }
        */
    }

    private void ChangeAlpha(float alpha)
    {
        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();
        foreach(var item in renderers)
        {
            ChangeAlpha(item, alpha);
        }
    }
    private void ChangeAlpha(MeshRenderer meshRenderer, float alpha)
    {
        Color newColor = meshRenderer.sharedMaterial.color;
        newColor.a = alpha;
        meshRenderer.sharedMaterial.color = newColor;
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical * 1.2f/* la caméra donne l'impression que c'est plus lent sinon*/);
        rb.velocity = movement * speed;

        rb.position = new Vector3
        (
            Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax)
        );

        rb.rotation = Quaternion.Euler(0.0f, -90.0f, rb.velocity.z * -tilt);

        if (ShouldFire() && Time.fixedTime >= nextPossibleFire)
        {
            Fire();
            int level = alchemyPoints[ElementType.AIR].level;
            nextPossibleFire = Time.fixedTime + (1f / airBonusDataList[level - 1].fireRate);
        }

        if(!shieldComponent.active && alchemyPoints[ElementType.WATER].value >= 50)
        {
            shieldComponent.Activate();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Time.time > invicibilityEnd)
        {
            invicibilityEnd = Time.time + 0.5f;
            var otherGameObject = other.gameObject;
            var otherDamageable = otherGameObject.GetComponent<Damageable>();
            if (otherGameObject.CompareTag("Enemy") && otherDamageable != null)
            {
                float damage = 1;
                otherDamageable.Damage(damage, true);
                damageable.Damage(damage, true);
            }
        }
    }

    bool ShouldFire()
    {
        return Input.GetButton("Fire");
    }

    void Fire()
    {
        int fireLevel = alchemyPoints[ElementType.FIRE].level;
        int index = Math.Min(fireBonusDataList.Count - 1, fireLevel - 1);
        
        FireBonus fireBonus = fireBonusDataList[index];
        for(int i=0; i < fireBonus.startPositions.Count; i++)
        {
            CreateLaser(fireBonus.startPositions[i], fireBonus.directions[i], 100f);
        }
        shotAudioSource.pitch = (UnityEngine.Random.Range(0.92f, 1.08f) + UnityEngine.Random.Range(0.92f, 1.08f)) / 2;
        shotAudioSource.Play();
    }

    void CreateLaser(Vector3 startPosition, Vector3 direction, float laserSpeed) { 
        GameObject laser = GameObject.Instantiate(regularLaser, transform.position + startPosition, Quaternion.identity, transform.root);
        LaserAI laserAi = laser.GetComponent<LaserAI>();
        laserAi.speed = laserSpeed;
        laserAi.sourceTag = "Player";
        laserAi.direction = direction;
        laserAi.color = Color.white;
    }

    public void AddAlchemyElement(ElementType type, int value)
    {
        //Debug.Log("Adding " + value + " " + type.ToString());
        if (type != ElementType.GOLD)
        {
            ElementData data;
            alchemyPoints.TryGetValue(type, out data);
            bool levelUp = data.Add(value);
            if (levelUp)
            {
                LevelUp(type, data);
            }
            bonusAudioSource.Play();
        }
        else
        {
            coinAudioSource.Play();
            currentGold += value;
        }
    }

    private void LevelUp(ElementType type, ElementData data)
    {
        if(type == ElementType.EARTH)
        {
            if(damageable.hitPoints < maxHitPoints)
            {
                data.UseElement(data.GetNextLevelValue());
                damageable.hitPoints++;
            }
        }
    }

    public float GetAlchemyElementValue(ElementType type)
    {
        ElementData data;
        alchemyPoints.TryGetValue(type, out data);
        return (float) data.value / data.GetNextLevelValue();
    }

    public int GetCurrentGold()
    {
        return currentGold;
    }

    public void Death()
    {
        DataManager.Instance.lastScore = currentGold;
        if(currentGold > DataManager.Instance.hightScore)
        {
            DataManager.Instance.hightScore = currentGold;
        }
        DataManager.Instance.gameOver = true;
        SceneManager.LoadScene("MainMenu");
    }

    [Serializable]
    public class FireBonus
    {
        public List<Vector3> startPositions = new List<Vector3>();
        public List<Vector3> directions = new List<Vector3>();
    }

    [Serializable]
    public class AirBonus
    {
        public float fireRate;
    }
}
