﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Ennemie 3 : je fonce sur le joueur.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class Enemy3Controller : AbstractEnemyController {

    public float reorientSpeed = 10.0f;
    public float reorientInterval = .2f;
    public float reorientSmoothTime = .1f;

    private float nextPossibleFire = 0f;
    private float nextReorient = 0f;

    // Use this for initialization
    void Start ()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");

        speed = Random.Range(minSpeed, maxSpeed);

        Vector3 direction;
        if (player)
        {
            direction = (player.transform.position - gameObject.transform.position).normalized;
        } else
        {
            direction = Vector3.left;
        }
        rigibody = GetComponent<Rigidbody>();
        rigibody.velocity = direction * speed;
    }

    private void FixedUpdate()
    {
        if (Time.fixedTime >= nextReorient)
        {
            Reorient();
            nextReorient = Time.fixedTime + reorientInterval;
        }
        if (Time.fixedTime >= nextPossibleFire && ShouldFire())
        {
            FireLaser(regularLaser, transform.position, rigibody.velocity);
            nextPossibleFire = Time.fixedTime + Random.Range(fireInterval / 2, fireInterval * 2);
        }
    }

    void Reorient()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");

        if (player)
        {
            var playerPosition = player.transform.position;
            var position = transform.position;
            var direction = playerPosition - position;

            if (direction.x < 0 && Vector3.Angle(Vector3.left, direction) <= 75f)
            {
                var velocity = rigibody.velocity.normalized;
                rigibody.velocity = Vector3.SmoothDamp(position, playerPosition, ref velocity, reorientSmoothTime);
                rigibody.velocity = velocity.normalized * speed;
            }
        }
    }

    bool ShouldFire()
    {
        var player = GameObject.FindGameObjectWithTag("Player");
        return player && (player.transform.position - transform.position).magnitude <= weaponRange;
    }

}
