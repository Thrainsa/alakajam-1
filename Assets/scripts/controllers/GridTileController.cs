﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridTileController : MonoBehaviour {

    public float initialSpeed = 10.0f;

    public float accelaration = 1f;

    public float resetOffset;

    public float minX;
    public Boundary alphaBoundaries;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(transform.position.x < minX)
        {
            transform.position = new Vector3(transform.position.x + resetOffset, transform.position.y, transform.position.z);
        }

        Material material = GetComponent<MeshRenderer>().material;
        float currentOpacity = material.color.a;
        if (transform.position.x < alphaBoundaries.xMin || transform.position.x > alphaBoundaries.xMax ||
            transform.position.z < alphaBoundaries.zMin || transform.position.z > alphaBoundaries.zMax)
        {
            material.color = new Color(1f, 1f, 1f, Mathf.Max(0.2f, currentOpacity - Time.deltaTime));
        }
        else
        {
            GetComponent<MeshRenderer>().material.color = new Color(1f, 1f, 1f, Mathf.Min(1.0f, currentOpacity +Time.deltaTime));
        }
    }

    void FixedUpdate()
    {
        transform.Translate(Vector3.left * initialSpeed * accelaration * Time.deltaTime);
    }
}
