﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Ennemie 1 : je fonce à une destination aléatoire !
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public abstract class AbstractEnemyController : MonoBehaviour, CanDie
{
    public float minSpeed = 10.0f;
    public float maxSpeed = 20.0f;
    public float fireInterval = 0.75f;
    public float weaponRange = 75;
    public GameObject regularLaser;
    public float laserSpeed = 25f;

    public List<SpawnElement> spawnElements = new List<SpawnElement>();

    protected float speed;
    protected Rigidbody rigibody;

    // Use this for initialization
    void Start () {
        speed = Random.Range(minSpeed, maxSpeed);
        rigibody = GetComponent<Rigidbody>();
    }

    public void AddSpeedBonus(float bonus)
    {
        speed += bonus;
        laserSpeed += bonus;
        fireInterval -= bonus / 100;
    }

    protected void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("GameArea"))
        {
            DestroyObject(gameObject);
        }
    }

    protected LaserAI FireLaser(GameObject projectilePrefab, Vector3 position, Vector3 direction)
    {
        GameObject laser = GameObject.Instantiate(projectilePrefab, position, Quaternion.identity, transform.root);
        LaserAI laserAi = laser.GetComponent<LaserAI>();

        laserAi.sourceTag = "Enemy";
        laserAi.direction = direction;
        laserAi.color = Color.red;
        laserAi.speed = laserSpeed;
        return laserAi;
    }

    public void Death()
    {
        SpawnElements(spawnElements);
        Destroy(gameObject);
    }

    protected void SpawnElements(List<SpawnElement> spawnElements)
    {
        foreach (SpawnElement spawnElement in spawnElements)
        {
            Instantiate(spawnElement.bonusComponentPrefab, transform.position, Quaternion.identity);
        }
    }
}
