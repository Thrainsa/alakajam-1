﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(BoxCollider))]
public class LaserAI : MonoBehaviour {

    public string sourceTag;
    public Vector3 direction;
    public float speed = 100f;
    public Color color;
    public float damage = 1.0f;

    private new Rigidbody rigidbody;

    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.rotation = Quaternion.LookRotation(direction);
        rigidbody.velocity = direction.normalized * speed;

        var renderer = gameObject.GetComponentInChildren<MeshRenderer>();
        renderer.material.color = color;
    }
	
	// Update is called once per frame
	void Update () {

    }

    private void FixedUpdate()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        var otherGameObject = other.gameObject;
        var otherDamageable = otherGameObject.GetComponent<Damageable>();

        if (!otherGameObject.CompareTag(sourceTag) && otherDamageable != null)
        {
            otherDamageable.Damage(damage, false);
            DestroyObject(gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("GameArea"))
        {
            DestroyObject(gameObject);
        }
    }

}
