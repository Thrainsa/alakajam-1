﻿using UnityEngine;

/// <summary>
/// Ennemie 1 : je fonce à une destination aléatoire !
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class Enemy1Controller : AbstractEnemyController
{
    private Vector3 direction;
    private float nextPossibleFire = 0f;

    // Use this for initialization
    void Start () {
        speed = Random.Range(minSpeed, maxSpeed);
        direction = new Vector3(-1, 0, Random.Range(-.1f, .1f)).normalized;
        rigibody = GetComponent<Rigidbody>();
        rigibody.velocity = direction * speed;
    }

    private void FixedUpdate()
    {
        if (Time.fixedTime >= nextPossibleFire && ShouldFire())
        {
            LaserAI laser = FireLaser(regularLaser, transform.position, direction);
            nextPossibleFire = Time.fixedTime + Random.Range(fireInterval * 2 / 3, fireInterval * 4 / 3);
        }
    }

    bool ShouldFire()
    {
        var player = GameObject.FindGameObjectWithTag("Player");
        var direction = player ? player.transform.position - transform.position : Vector3.left;

        return player && direction.x <= 0 && direction.magnitude <= weaponRange;
    }
}
