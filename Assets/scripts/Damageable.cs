﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageable : MonoBehaviour
{

    public float hitPoints = 1f;

    public bool isDead = false;

    private ElementData waterShield;
    private ShieldComponent shieldComponent;

    private AudioSource hitAudioSource;
    private AudioSource hurtAudioSource;
    private List<AudioSource> musics = new List<AudioSource>();

    // Use this for initialization
    void Start()
    {
        shieldComponent = GetComponentInChildren<ShieldComponent>();
        hurtAudioSource = GameObject.FindGameObjectWithTag("hurtAudio").GetComponent<AudioSource>();
        hitAudioSource = GameObject.FindGameObjectWithTag("hitAudio").GetComponent<AudioSource>();
        GameObject[] musicObjects = GameObject.FindGameObjectsWithTag("music");
        foreach (var item in musicObjects)
        {
            musics.Add(item.GetComponent<AudioSource>());
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Damage(float amount, bool collision)
    {
        if (waterShield != null && shieldComponent.active)
        {
            waterShield.UseElement(50);
            if (waterShield.value == 0)
            {
                shieldComponent.Deactivate();
            }
            hurtAudioSource.Play();
            return;
        }

        PlayerController playerController = GetComponent<PlayerController>();
        if (playerController != null)
        {
            foreach (var item in musics)
            {
                if (item.isPlaying)
                {
                    item.Pause();
                    StartCoroutine("UnPause", item);
                }
            }
            hurtAudioSource.Play();
            playerController.Hurt();
        }

        hitPoints -= amount;

        //Debug.LogFormat("{0} damaged by {1}, {2} HP remaining", gameObject, amount, hitPoints);

        if (hitPoints <= 0 && !isDead)
        {
            isDead = true;
            CanDie canDie = GetComponent<CanDie>();
            if (canDie != null)
            {
                if (!collision)
                {
                    hitAudioSource.Play();
                }
                canDie.Death();
            }
        }
    }

    public void SetWaterShield(ElementData waterShield)
    {
        this.waterShield = waterShield;
    }

    IEnumerator UnPause(AudioSource item)
    {
        yield return new WaitForSeconds(0.5f);
        item.UnPause();
        yield return null;
    }
}
