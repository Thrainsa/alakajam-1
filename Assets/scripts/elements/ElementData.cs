﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementData {

    public int value = 0;
    public int level = 1;

    public int maxLevel = 1;

    public int GetNextLevelValue()
    {
        return 100 * level;
    }

    public bool Add(int points)
    {
        value += points;
        if(value >= GetNextLevelValue())
        {
            if (level < maxLevel)
            {
                value -= GetNextLevelValue();
                level++;
            }
            else
            {
                value = GetNextLevelValue();
            }
            return true;
        }
        return false;
    }

    public void UseElement(int points)
    {
        value -= Math.Min(value, points);
    }
}
