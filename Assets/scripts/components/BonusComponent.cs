﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusComponent : MonoBehaviour {

    public int value;
    public ElementType type;

    public float initialSpeed = 10.0f;

    public float accelaration = 1f;

    public float explosionForce;
    public Vector3 explosionDirection;
    public float startTime;
    public bool attractedToPlayer = false;
    private Rigidbody r;
    private Transform player;

    // Use this for initialization
    void Start () {
        explosionDirection = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f));
        // Random position
        transform.position = transform.position + explosionDirection;
        explosionDirection = explosionDirection.normalized;
        startTime = Time.time;
        explosionForce = Random.Range(5f, 10f);
        r = GetComponent<Rigidbody>();
        r.velocity = explosionDirection * initialSpeed;
        r.AddExplosionForce(explosionForce, transform.position - explosionDirection, 10);
        PlayerController playerController = FindObjectOfType<PlayerController>();
        if (playerController != null) player = playerController.transform;
    }

    // Update is called once per frame
    /*void Update () {
        float delta = Time.time - startTime;
        if (delta < 2 && delta > 0)
        {
            r.AddForce(explosionDirection * explosionForce * Mathf.Pow((2f - delta) / 2f, 2) * Time.deltaTime);
        }
    }*/

     void FixedUpdate()
     {
         transform.position += Vector3.left * accelaration * Time.fixedDeltaTime;
        if (attractedToPlayer && player != null)
        {
            if (Vector3.Distance(transform.position, player.position) < 15)
            {
                transform.position += (player.position - transform.position) / 12;
            }
        }
     }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().AddAlchemyElement(type, value);
            Destroy(gameObject);
        }
    }
}

