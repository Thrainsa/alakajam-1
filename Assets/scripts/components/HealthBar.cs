﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    private Image image;
    private int health = 0;

	// Use this for initialization
	void Start () {
        image = GetComponent<Image>();
    }

    // Update is called once per frame
    public void HealthChange(int newHealth) {
        if (health != newHealth)
        {
            health = Mathf.Max(0, newHealth);
            image.material.mainTextureScale = new Vector2(health, 1);
            image.rectTransform.sizeDelta = new Vector2(20 * health, 20);
        }
    }
}
