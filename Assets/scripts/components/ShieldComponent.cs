﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldComponent : MonoBehaviour {

    private MeshRenderer shieldRenderer;
    public bool active;

	// Use this for initialization
	void Start () {
        shieldRenderer = GetComponentInChildren< MeshRenderer>();
        if (!active)
        {
            Deactivate();
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (active)
        {
            float alpha = (Mathf.Sin(Time.time * 3) + 1) / 4 + 0.25f;
            ChangeAlpha(alpha);
        }
	}

    private void ChangeAlpha(float alpha)
    {
        Color newColor = shieldRenderer.material.GetColor("_TintColor");
        newColor.a = alpha;
        shieldRenderer.sharedMaterial.SetColor("_TintColor", newColor);
    }

    public void Deactivate()
    {
        active = false;
        ChangeAlpha(0);
    }

    public void Activate()
    {
        active = true;
    }
}
