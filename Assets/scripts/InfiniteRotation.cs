﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteRotation : MonoBehaviour {

    public bool random;

    public Vector3 rotation;

    public float rotationSpeed = 2f;

    // Use this for initialization
    void Start () {
        if (random)
        {
            rotation = Random.onUnitSphere;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(rotation * Time.deltaTime * rotationSpeed);
    }
}
