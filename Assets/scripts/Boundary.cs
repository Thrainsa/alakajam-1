﻿using System;

[Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;

    public Boundary(float xMin, float xMax, float zMin, float zMax)
    {
        this.xMin = xMin;
        this.xMax = xMax;
        this.zMin = zMin;
        this.zMax = zMax;
    }
}
