﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour {

    public GameObject welcomeScreen;
    public GameObject gameOverScreen;

    public Text hightScore;
    public Text hightScore2;
    public GameObject hightScoreToggle;
    public Text lastScore;

    // Use this for initialization
    void Start () {
        if (DataManager.Instance.gameOver)
        {
            welcomeScreen.SetActive(false);
            gameOverScreen.SetActive(true);
        }
        else
        {
            welcomeScreen.SetActive(true);
            gameOverScreen.SetActive(false);
        }
        hightScore.text = String.Format("{0:N0}", DataManager.Instance.hightScore);
        hightScore2.text = String.Format("{0:N0}", DataManager.Instance.hightScore);
        hightScoreToggle.SetActive(DataManager.Instance.hightScore > 0);
        lastScore.text = String.Format("{0:N0}", DataManager.Instance.lastScore);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartGame()
    {
        SceneManager.LoadScene("main");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
