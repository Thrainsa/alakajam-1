﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Informations permettant à un EnemySpawner d'instancier les ennemies automatiquement.
/// </summary>
[Serializable]
public class EnemySpawnInfo {

    /// <summary>
    /// Nom de la catégorie.
    /// </summary>
    public String name;

    /// <summary>
    /// Type d'ennemie à instancier.
    /// </summary>
    public GameObject enemy;

    /// <summary>
    /// Vague à partir de laquelle apparaît l'ennemie.
    /// </summary>
    public int minWave = 0;

    /// <summary>
    /// Probabilité de voir apparaître ce type d'ennemie dans une vague.
    /// </summary>
    public float probability = 1f;

    /// <summary>
    /// Combien un groupe de ce type d'ennemie contiendra-t-il d'ennemie au minimum.
    /// </summary>
    public int groupMin = 1;

    /// <summary>
    /// Combien un groupe de ce type d'ennemie contiendra-t-il d'ennemie au maximum.
    /// </summary>
    public int groupMax = 10;

    /// <summary>
    /// Nombre d'ennemies additionnels dans un groupe par vague (arrondi).
    /// </summary>
    public float groupIncrementPerWave = 1f;

    /// <summary>
    /// Temps entre deux ennemies dans le même groupe.
    /// </summary>
    public float withinGroupRate = 0.2f;

}
