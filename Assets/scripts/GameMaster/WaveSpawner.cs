﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using NetRandom = System.Random;
using UniRandom = UnityEngine.Random;

/// <summary>
/// Script de gestion de l'entité responsable de la génération des entités ennemies.
/// </summary>
public class WaveSpawner : MonoBehaviour {

    public const string TagEnemies = "Enemy";
    public const float PodSummonInterval = 0.01f;

    public Text waveNumberLabel;

    /// <summary>
    /// Seed pour la RNG.
    /// </summary>
    public int seed = 0xBAFFE;

    /// <summary>
    /// Nombre de groupes d'ennemies par vague (arrondi).
    /// </summary>
    public float podsPerWave = 3.24f;

    /// <summary>
    /// Temps entre deux groupes.
    /// </summary>
    public float timeBetweenPods = 0f;

    /// <summary>
    /// Nombre de secondes avant de démarrer une nouvelle vague.
    /// </summary>
    public float waveCountDown = 3f;

    /// <summary>
    /// Intervalle entre deux vérifications de fin de vague.
    /// </summary>
    public float waveCheckInterval = .5f;

    /// <summary>
    /// Catégorie d'ennemies.
    /// </summary>
    public EnemySpawnInfo[] enemies;

    /// <summary>
    /// Points de génération d'ennemies.
    /// </summary>
    public Transform[] spawnPoints;

    private WaveState state = WaveState.WAITING;

    /// <summary>
    /// Numéro de la prochaine vague
    /// </summary>
    private int nextWave = 1;

    private float nextWaveStart = 0f;

    private Coroutine coroutine;

    private float nextWaveCheck = 0f;
    private bool enemiesAlive;

    private void Start()
    {
        if (enemies.Length == 0)
        {
            Debug.LogError("Des descriptions d'ennemies sont nécessaires pour en invoquer !");
        }
        if (spawnPoints.Length == 0)
        {
            Debug.LogError("Des spawn points sont nécessaires pour invoquer des ennemies !");
        }
        waveNumberLabel.text = "0";
    }

    private void FixedUpdate()
    {
        if (Time.fixedTime >= nextWaveCheck)
        {
            enemiesAlive = IsEnemiesAlive();
            nextWaveCheck = Time.fixedTime + waveCheckInterval;
        }
        
        switch (state)
        {
            case WaveState.COUNTING_DOWN:
                if (Time.fixedTime >= nextWaveStart)
                {
                    state = WaveState.SPAWNING;
                }
                break;

            case WaveState.SPAWNING:
                if (coroutine == null)
                {
                    coroutine = StartCoroutine(SendWave(nextWave++));
                    enemiesAlive = true;
                    state = WaveState.WAITING;
                }
                break;

            case WaveState.WAITING:
                if (!enemiesAlive)
                {
                    waveNumberLabel.text = nextWave.ToString();
                    nextWaveStart = Time.fixedTime + (waveCountDown - nextWave * 0.1f);
                    state = WaveState.COUNTING_DOWN;
                    // Debug.LogFormat("Démarrage de la vague {0} dans {1} s", nextWave, waveCountDown);
                }
                break;
        }
    }

    private bool IsEnemiesAlive()
    {
        return GameObject.FindGameObjectsWithTag(TagEnemies).Length > 0;
    }

    private IEnumerator SendWave(int waveId)
    {
        Debug.LogFormat("Démarrage de la vague {0}", waveId);

        var random = new NetRandom(seed * (1 + waveId));

        var waveEnemies = enemies.Where(enemy => waveId >= enemy.minWave).Where(enemy => random.NextDouble() < enemy.probability).ToArray();
        if (waveEnemies.Count() > 0)
        {
            int podCount = (int)Math.Round(podsPerWave + waveId * 2);
            var pods = Enumerable.Range(0, podCount).Select(podId =>
            {
                int enemyType = UniRandom.Range(0, waveEnemies.Length);
                EnemySpawnInfo waveEnemy = waveEnemies[enemyType];
                var pod = new WavePod
                {
                    id = podId,
                    enemy = waveEnemy,
                    nextEnemy = Time.fixedTime + podId * timeBetweenPods,
                    remainingEnemies = Mathf.Clamp(waveEnemy.groupMin + (int)Math.Round((waveId - waveEnemy.minWave) * waveEnemy.groupIncrementPerWave), waveEnemy.groupMin, waveEnemy.groupMax),
                    spawnPoint = spawnPoints[UniRandom.Range(0, spawnPoints.Length - 1)]
                };

                Debug.LogFormat("Groupe {0} : {1} ennemi(s) de type {2}", podId, pod.remainingEnemies, enemyType);

                return pod;
            }).ToArray();

            // Invocation des groupes.
            bool allDone = false;
            while (!allDone)
            {
                var remainingPods = pods.Where(pod => pod.remainingEnemies > 0).ToArray();
                foreach (var pod in remainingPods)
                {
                    //Debug.LogFormat("Groupe {0} > {1}", pod.id, pod.nextEnemy);
                    if (Time.fixedTime >= pod.nextEnemy)
                    {
                       // Debug.LogFormat("Groupe {0} : spawn ennemi de type {1}", pod.id, pod.enemy.name);
                        var enemy = pod.enemy;
                        var enemyObject = Instantiate(enemy.enemy, pod.spawnPoint);
                        AbstractEnemyController enemyController = enemyObject.GetComponent<AbstractEnemyController>();
                        if (nextWave > 5)
                        {
                            enemyController.AddSpeedBonus(nextWave - 5);
                        }

                        pod.nextEnemy = Time.fixedTime + enemy.withinGroupRate;
                        pod.remainingEnemies--;
                    }
                }

                allDone = remainingPods.Count() == 0;
                yield return new WaitForSeconds(PodSummonInterval);
            }

            coroutine = null;
        }
    }

}

enum WaveState
{
    COUNTING_DOWN,
    SPAWNING,
    WAITING
}

[Serializable]
class WavePod
{
    public int id;
    public EnemySpawnInfo enemy;
    public float nextEnemy;
    public int remainingEnemies;
    public Transform spawnPoint;

}
