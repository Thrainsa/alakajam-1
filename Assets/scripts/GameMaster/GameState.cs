﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GameState : MonoBehaviour {

    private PlayerController player;

    public Scrollbar fireScrollbar;
    public Scrollbar airScrollbar;
    public Scrollbar waterScrollbar;
    public Scrollbar earthScrollbar;

    public Text goldText;
    public HealthBar healthBar;

    public AudioSource musicAudioSource;

    // Use this for initialization
    void Start () {
        player = FindObjectOfType<PlayerController>();
    }
	
	// Update is called once per frame
	void Update () {
        fireScrollbar.size = player.GetAlchemyElementValue(ElementType.FIRE);
        airScrollbar.size = player.GetAlchemyElementValue(ElementType.AIR);
        waterScrollbar.size = player.GetAlchemyElementValue(ElementType.WATER);
        earthScrollbar.size = player.GetAlchemyElementValue(ElementType.EARTH);
        goldText.text = String.Format("{0:N0}", player.GetCurrentGold());
        healthBar.HealthChange((int) player.GetComponent<Damageable>().hitPoints);
    }
}
