﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public int hightScore = 0;
    public int lastScore = 0;
    public bool mute = false;

    private static DataManager instance;
    public bool gameOver = false;

    public static DataManager Instance
    {
        get
        {
            if (instance == null)
            {
                throw new System.InvalidOperationException("The singleton instance of DataManager must be "
                    + "initialized by the Unity Engine (i.e. the GameState prefab must be on every scene)");
            }
            return instance;
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
}
